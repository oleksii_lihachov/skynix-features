<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 *
 * @wordpress-plugin
 * Plugin Name:       Skynix Features
 * Plugin URI:        https://bitbucket.org/oleksii_lihachov/skynix-features/src/master
 * Description:       WordPress Plugin for implementation Tel Aviv night club site features.
 * Version:           1.0.0
 * Author:            Skynix
 * Author URI:        http://skynix.co
 * License:           MIT
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sk-features
 * Domain Path:       /languages
 */


namespace SKPlugin;

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// ======================  CONSTANTS ===================================
define( 'PLUGIN_NAME_VERSION', '1.0.0' );
define( 'SK_PLUGIN_SLUG', 'skynix-features' );
define( 'SK_PLUGIN_DIR', plugin_dir_path( dirname( __FILE__ ) ) . SK_PLUGIN_SLUG . '/' );
define( 'SK_PLUGIN_URL', plugin_dir_url( dirname( __FILE__ ) ) . SK_PLUGIN_SLUG . '/' );
define( 'SK_LOG_DIR', SK_PLUGIN_DIR . 'logs/' );

$loader = require SK_PLUGIN_DIR . 'vendor/autoload.php';
$loader->addPsr4( 'SPSRPlugin\\', __DIR__ );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-sk-features.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_sk_features() {
	$plugin = new SKFeatures();
	$plugin->run();
}

try {
	run_sk_features();
} catch ( \Exception $e ) {
	if ( is_writable( SK_LOG_DIR ) ) {
		file_put_contents( SK_LOG_DIR . 'error_run.txt', $e );
	}
}

/**
 * The code that runs during plugin activation.
 */
register_activation_hook( __FILE__, function () {
	flush_rewrite_rules();

	if ( is_writable( SK_LOG_DIR ) ) {
		file_put_contents( SK_LOG_DIR . 'error_activation.txt', ob_get_contents() );
	}
} );

/**
 * The code that runs during plugin deactivation.
 */
register_deactivation_hook( __FILE__, function () {
	flush_rewrite_rules();
} );